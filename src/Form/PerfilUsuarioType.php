<?php


namespace App\Form;


use App\Entity\PerfilUsuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PerfilUsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('whatsapp', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('direccion', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('pais', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('urlFacebook', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('urlInstagram', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('urlYoutube', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PerfilUsuario::class,
        ]);
    }
}