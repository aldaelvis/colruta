<?php


namespace App\Form;


use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends BaseType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $user = new User();
        $builder
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'expanded' => false,

                'choices' => [
                    'Revendedor' => 'ROLE_REVENDEDOR',
                    'Cliente' => 'ROLE_CLIENT',
                    'Administrador' => 'ROLE_ADMIN'
                ]
            ])
            ->add('email', EmailType::class)
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => 'Contraseña'
                ],
                'second_options' => [
                    'label' => 'Repita la contraseña'
                ]
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Nombres',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Apellidos',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('isActive', null, [
                'label' => 'Estado'
            ])
            ->add('perfil', PerfilUsuarioType::class);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class
        ]);
    }

}