<?php

namespace App\Form;

use App\Entity\Tutorial;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class TutorialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('descripcion', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
/*Mio*/
            ->add('youtube', null, [
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
/*Fin Mio*/
            ->add('url', FileType::class, [
                'label' => 'Video',
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'accept' => 'video/*'
                ],
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'video/*'
                        ],
                        'mimeTypesMessage' => 'Por favor suba un video con extension válida',
                    ])
                ],
            ])
            ->add('estado', null, [
                'attr' => [
                    'class' => ''
                ],

            ])
            ->add('category', null, [
                'label' => 'Categoría',
                'attr' => [
                    'class' => 'form-control'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tutorial::class,
        ]);
    }
}
