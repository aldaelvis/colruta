<?php

namespace App\Controller;

use App\Entity\Mensajes;
use App\Entity\User;
use App\Repository\MensajesRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class MensajeController extends AbstractController
{
    /**
     * @Route("admin/message", name="app_message")
     */
    public function index(UserRepository $userRepository)
    {

        $users = $userRepository->findBy([

        ], ['id' => 'DESC']);

        return $this->render('mensaje/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("admin/message/{id}", name="app_message_show")
     */
    public function showMessages(UserRepository $userRepository, MensajesRepository $mensajesRepository, $id)
    {
        $user = $userRepository->find($id);
        $mensajes = $mensajesRepository->findBy([
            'user' => $user,
        ], ['id' => 'DESC']);

        return $this->render('mensaje/messages_descripcion.html.twig', [
            'user' => $user,
            'mensajes' => $mensajes,
        ]);
    }

    /**
     * @Route("admin/message/{id}/nuevo", name="app_message_nuevo")
     */
    public function nuevoMessage(UserRepository $userRepository, MensajesRepository $mensajesRepository,
                                 EntityManagerInterface $entityManager, Request $request, $id)
    {
        $user = $userRepository->find($id);

        if ($request->getMethod() === Request::METHOD_POST) {
            $titulo = $request->request->get('titulo');
            $descripcion = $request->request->get('descripcion');
            $remitente = $request->request->get('remitente');

            if (empty($titulo) || empty($descripcion) || empty($remitente)) {
                $this->addFlash('error', 'Los campos (*) son obligatorios 🙁');
            } else {
                $message = new Mensajes();
                $message->setTitulo($titulo);
                $message->setDescripcion($descripcion);
                $message->setUser($user);
                $message->setEstado(true);
                $message->setRemitente($remitente);
                $entityManager->persist($message);
                $entityManager->flush();

                $this->addFlash('success', 'Mensaje enviado correctamente 🙂');
            }
        }

        return $this->render('mensaje/messages_nuevo.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route ("/notificaciones", name="notificaciones")
     */
    public function mostrarNotificaciones(MensajesRepository $mensajesRepository, SerializerInterface $serializer)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();
        $mensajes = $mensajesRepository->findByMessageUser($user->getId());
        $json = $serializer->serialize($mensajes, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route ("/notificaciones/visto", name="notificaciones_visto")
     */
    public function vistoNotificaciones(EntityManagerInterface $entityManager, MensajesRepository $mensajesRepository, SerializerInterface $serializer)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();
        $mensajes = $mensajesRepository->findByMessageUser($user->getId());

        $data = ["mensaje" => "Visto todos los mensajes"];
        foreach ($mensajes as $value) {
            $m = $mensajesRepository->find($value['id']);
            $m->setEstado(false);
            $entityManager->persist($m);
            $entityManager->flush();
        }

        $json = $serializer->serialize($data, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }
}
