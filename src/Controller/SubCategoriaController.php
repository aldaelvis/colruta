<?php

namespace App\Controller;

use App\Entity\SubCategoria;
use App\Form\SubCategoriaType;
use App\Repository\SubCategoriaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sub/categoria")
 */
class SubCategoriaController extends AbstractController
{
    /**
     * @Route("/", name="sub_categoria_index", methods={"GET"})
     */
    public function index(SubCategoriaRepository $subCategoriaRepository): Response
    {
        return $this->render('sub_categoria/index.html.twig', [
            'sub_categorias' => $subCategoriaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sub_categoria_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $subCategorium = new SubCategoria();
        $form = $this->createForm(SubCategoriaType::class, $subCategorium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($subCategorium);
            $entityManager->flush();

            return $this->redirectToRoute('sub_categoria_index');
        }

        return $this->render('sub_categoria/new.html.twig', [
            'sub_categorium' => $subCategorium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sub_categoria_show", methods={"GET"})
     */
    public function show(SubCategoria $subCategorium): Response
    {
        return $this->render('sub_categoria/show.html.twig', [
            'sub_categorium' => $subCategorium,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sub_categoria_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SubCategoria $subCategorium): Response
    {
        $form = $this->createForm(SubCategoriaType::class, $subCategorium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sub_categoria_index');
        }

        return $this->render('sub_categoria/edit.html.twig', [
            'sub_categorium' => $subCategorium,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sub_categoria_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SubCategoria $subCategorium): Response
    {
        if ($this->isCsrfTokenValid('delete'.$subCategorium->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($subCategorium);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sub_categoria_index');
    }
}
