<?php

namespace App\Controller;

use App\Entity\Plan;
use App\Form\PlanType;
use App\Repository\PlanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PlanController extends AbstractController
{
    /**
     * @Route("admin/plan", name="plan")
     * @param EntityManagerInterface $entityManager
     * @param PlanRepository $repository
     * @param Request $request
     * @return Response
     */
    public function index(EntityManagerInterface $entityManager, PlanRepository $repository, Request $request)
    {
        $plan = $repository->findBy([], ['precio' => 'ASC']);

        if ($request->getMethod() === Request::METHOD_POST) {
            $precio = $request->request->get('precio');
            $cantidad = $request->request->get('cantidad');
            $id = $request->request->get('id');
            if (empty($precio) || $precio <= 0 || empty($cantidad)) {
                $this->addFlash('error', 'Los campos en (*) son obligatorios 🙁');
            } else {
                if (($precio > 0) && (!empty($id) && $id > 0)) {
                    // Update
                    $plan = $repository->find($id);
                    $plan->setPrecio($precio);
                    $plan->setNumeroClientes($cantidad);
                    $this->addFlash('success', 'La información se actualizo correctamente 🙂');
                } else {
                    // Insert
                    $plan = new Plan();
                    $plan->setNombre('normal');
                    $plan->setPrecio($precio);
                    $plan->setNumeroClientes($cantidad);
                    $this->addFlash('success', 'El información se guardo correctamente 🙂');
                }

                $entityManager->persist($plan);
                $entityManager->flush();
            }
        }

        return $this->render('plan/index.html.twig', [
            'planes' => $plan
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     * @Route ("admin/plan/nuevo", name="admin_plan_nuevo")
     */
    public function createPlan(EntityManagerInterface $entityManager, Request $request)
    {
        $form = $this->createForm(PlanType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $plan = $form->getData();
            $entityManager->persist($plan);
            $entityManager->flush();

            $this->addFlash('success', 'Se guardo correctamente el plan 😃');
            return $this->redirectToRoute('plan');
        }

        return $this->render('plan/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param Plan $plan
     * @Route ("admin/plan/{id}/editar", name="admin_plan_editar")
     * @return RedirectResponse|Response
     */
    public function editPlan(EntityManagerInterface $entityManager, Request $request, Plan $plan)
    {
        $form = $this->createForm(PlanType::class, $plan);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($plan);
            $entityManager->flush();

            $this->addFlash('success', 'Se edito correctamente 😃');
            return $this->redirectToRoute('plan');
        }

        return $this->render('plan/editar.html.twig', [
            'form' => $form->createView(),
            'plan' => $plan,
        ]);
    }

    /**
     * @param Request $request
     * @param Plan $plan
     * @return Response
     * @Route ("admin/plan/{id}/delete", name="admin_plan_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Plan $plan): Response
    {
        if ($this->isCsrfTokenValid('delete' . $plan->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($plan);
            $entityManager->flush();
            $this->addFlash('success', 'Se elimino correctamente 😃');
        }

        return $this->redirectToRoute('plan');
    }
}
