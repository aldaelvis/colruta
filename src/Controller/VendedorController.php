<?php


namespace App\Controller;


use App\Entity\Cliente;
use App\Entity\Cuenta;
use App\Entity\Mensajes;
use App\Entity\User;
use App\Repository\ClienteRepository;
use App\Repository\CuentaRepository;
use App\Repository\DetalleCuentaRepository;
use App\Repository\PlanRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class VendedorController extends AbstractController
{
    /**
     * @Route ("admin/vendedor/{page<[1-9]\d*>}", name="app_vendedor")
     */
    public function index(UserRepository $userRepository, $page = 1)
    {
        $status = null;
        $email = null;

        $paginator = $userRepository->findAllVendedores($page, $email, $status);

        return $this->render('user/vendedor.html.twig', [
            "paginator" => $paginator
        ]);
    }

    /**
     * @Route ("admin/vendedor/cuentas/{id}", name="app_vendedor_cuentas")
     */
    public function showCuentas(UserRepository $userRepository, CuentaRepository $cuentaRepository, int $id)
    {
        $user = $userRepository->find($id);
        $cuentas = $cuentaRepository->findBy([
            'user' => $user
        ], ['vencimiento' => 'DESC']);

        return $this->render('user/vendedor_cuenta.html.twig', [
            "user" => $user,
            "cuentas" => $cuentas
        ]);
    }


    /**
     * @Route ("admin/vendedor/cuentas/{id}/nuevo", name="app_vendedor_cuentas_nuevo")
     */
    public function newCuenta(UserRepository $userRepository, PlanRepository $planRepository, Request $request,
                              EntityManagerInterface $entityManager, int $id)
    {
        /** @var Cuenta $cuenta */
        $user = $userRepository->find($id);
        $plan = $planRepository->findOneBy([], ['id' => 'DESC']);

        $saldoVendedor = $user->getSaldo();
        if ($saldoVendedor < $plan->getPrecio()) {
            $pass = false;
        } else {
            $pass = true;
        }

        if ($request->getMethod() === Request::METHOD_POST) {
            $username = $request->request->get('username');
            $password = $request->request->get('password');
            if ($pass) {
                if (empty($username) || empty($password)) {
                    $this->addFlash('error', 'Los campos marcados con (*) son obligatorios 🙁');
                } else {
                    $entityManager->getConnection()->beginTransaction();
                    try {
                        $message = new Mensajes();
                        $message->setRemitente('Administrador');
                        $message->setEstado(true);
                        $message->setUser($user);
                        $message->setDescripcion('Usuario: ' . $username . ' Password: ' . $password);
                        $message->setTitulo("Claves de acceso de la nueva cuenta");
                        $entityManager->persist($message);
                        $user->setSaldo($user->getSaldo() - $plan->getPrecio());
                        $entityManager->persist($user);
                        $cuenta = new Cuenta();
                        $cuenta->setUsername($username);
                        $cuenta->setPassword($password);
                        $cuenta->setRestanteCliente($plan->getNumeroClientes());
                        $cuenta->setUser($user);
                        $cuenta->setPlan($plan);
                        $cuenta->setCreated(new \DateTime());
                        $date = new \DateTime();
                        $cuenta->setVencimiento($date->modify('+30 days'));
                        $entityManager->persist($cuenta);
                        $entityManager->flush();
                        $entityManager->getConnection()->commit();
                        $this->addFlash('success', 'Cuenta guardado correctamente 🙂');
                        return $this->redirectToRoute('app_vendedor_cuentas', [
                            'id' => $user->getId()
                        ]);
                    } catch (\Exception $e) {
                        $entityManager->rollback();
                        throw $e;
                    }
                }
            } else {
                $this->addFlash('error', 'El saldo de este cliente no es suficiente segun sus configuraciones 🙂');
            }

        }

        return $this->render('user/vendedor_cuenta_nuevo.html.twig', [
            "user" => $user,
            'plan' => $plan,
            'pass' => $pass,
        ]);
    }

    /**
     * @Route ("admin/vendedor/cuentas/{iduser}/editar/{idcuenta}", name="app_vendedor_cuentas_editar")
     */
    public function editCuenta(UserRepository $userRepository, CuentaRepository $cuentaRepository, PlanRepository $planRepository,
                               EntityManagerInterface $entityManager, Request $request, int $iduser, int $idcuenta)
    {

        $user = $userRepository->find($iduser);
        $cuenta = $cuentaRepository->find($idcuenta);
        $plan = $planRepository->findOneBy([], ['id' => 'DESC']);

        if ($request->getMethod() === Request::METHOD_POST) {
            $username = $request->request->get('username');
            $password = $request->request->get('password');

            if (empty($username) || empty($password)) {
                $this->addFlash('error', 'Los campos marcados con (*) son obligatorios 🙁');
            } else {
                $cuenta->setUsername($username);
                $cuenta->setPassword($password);
                $entityManager->persist($cuenta);
                $entityManager->flush();

                $this->addFlash('success', 'Cuenta actualizado correctamente 🙂');

                return $this->redirectToRoute('app_vendedor_cuentas', [
                    'id' => $user->getId()
                ]);
            }
        }

        return $this->render('user/vendedor_cuenta_editar.html.twig', [
            'user' => $user,
            'cuenta' => $cuenta,
            'plan' => $plan,
        ]);
    }

    /**
     * @param CuentaRepository $cuentaRepository
     * @return Response
     * @Route ("/admin/pedidos/cuentas", name="pedidos_cuentas")
     */
    public function pedidoCuenta(CuentaRepository $cuentaRepository)
    {
        $cuentas = $cuentaRepository->findCuentasSolicitadas();

        return $this->render('pedidos/nuevas_cuentas.html.twig', [
            'cuentas' => $cuentas,
        ]);
    }

    /**
     * @param CuentaRepository $cuentaRepository
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param $id
     * @return Response
     * @Route ("/admin/pedidos/cuenta/{id}", name="pedidos_cuenta_activar")
     * @throws ConnectionException
     */
    public function activarCuentaSolicitada(CuentaRepository $cuentaRepository,
                                            EntityManagerInterface $entityManager, Request $request, $id)
    {

        $cuenta = $cuentaRepository->find($id);
        $user = $cuenta->getUser();

        if ($request->getMethod() === Request::METHOD_POST) {
            //--persistir
            $username = $request->request->get('username');
            $password = $request->request->get('password');
            if (empty($username) || empty($password)) {
                $this->addFlash('error', 'Los campos marcados con (*) son obligatorios 🙁');
            } else {
                $entityManager->getConnection()->beginTransaction();
                try {
                    $message = new Mensajes();
                    $message->setRemitente('Administrador');
                    $message->setEstado(true);
                    $message->setUser($user);
                    $message->setDescripcion('Usuario: ' . $username . ' Password: ' . $password);
                    $message->setTitulo("Claves de acceso de la nueva cuenta");
                    $entityManager->persist($message);

                    $cuenta->setUsername($username);
                    $cuenta->setPassword($password);
                    $cuenta->setEstado(true);
                    $cuenta->setCreated(new \DateTime());
                    $date = new \DateTime();
                    $cuenta->setVencimiento($date->modify('+30 days'));
                    $entityManager->persist($cuenta);
                    $entityManager->flush();

                    $entityManager->getConnection()->commit();
                    $this->addFlash('success', 'Se activo correctamente la cuenta 😃');

                    return $this->redirectToRoute('pedidos_cuentas');
                } catch (\Exception $e) {
                    $entityManager->rollback();
                    throw $e;
                }
            }
        }

        return $this->render('pedidos/activar_cuenta.html.twig', [
            'cuenta' => $cuenta,
        ]);
    }

    /**
     * @param Request $request
     * @param Cuenta $cuenta
     * @return RedirectResponse
     * @Route ("admin/pedidos/cuenta/{id}/delete", name="pedidos_cuenta_eliminar")
     */
    public function eliminarPedidoCuenta(Request $request, Cuenta $cuenta)
    {
        if ($this->isCsrfTokenValid('delete' . $cuenta->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cuenta);
            $entityManager->flush();

            $this->addFlash('success', 'Se elimino el pedido de cuenta correctamente 🙂');
        }

        return $this->redirectToRoute('pedidos_cuentas');
    }

    // METODOS PARA EL PANEL DEL VENDEDOR
    // ----------------------------------

    /**
     * @Route ("/cuentas", name="app_vendedorn")
     */
    public function listCuentasVendedor(PlanRepository $planRepository, CuentaRepository $cuentaRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();
        $plan = $planRepository->findOneBy([], ['id' => 'DESC']);

        $cuentas = $cuentaRepository->findBy([
            'user' => $user
        ], ['id' => 'DESC']);

        return $this->render('default/cuentas.html.twig', [
            'cuentas' => $cuentas,
            'user' => $user,
            'plan' => $plan,
        ]);
    }

    /**
     * @Route ("/cuentas/clientes/{id}", name="app_vendedorn_clientes")
     */
    public function listClientCuentasVendedor(CuentaRepository $cuentaRepository, DetalleCuentaRepository $detalleCuentaRepository,
                                              int $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $cuenta = $cuentaRepository->find($id);
        $detalles = $detalleCuentaRepository->findByCuenta($id);

        return $this->render('default/listado_clientes.html.twig', [
            'cuenta' => $cuenta,
            'detalles' => $detalles,
        ]);
    }

    /**
     * @Route ("/cuentas/{idcuenta}/cliente/{id}", name="app_vendedorn_cliente_email", methods={"POST"})
     */
    public function enviarEmail(ClienteRepository $clienteRepository, \Swift_Mailer $mailer, Request $request, $id, $idcuenta)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $cliente = $clienteRepository->find($id);

        if ($this->isCsrfTokenValid('envio' . $cliente->getId(), $request->request->get('_token'))) {
            $message = (new \Swift_Message('Accesos para tu cuenta'))
                ->setFrom('info@4kgoiptv.com')
                ->setTo($cliente->getEmail())
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'emails/message.html.twig',
                        [
                            'email' => $cliente->getCliente(),
                            'password' => $cliente->getPassword()
                        ]
                    ),
                    'text/html'
                );
            $mailer->send($message);

            $this->addFlash('success', 'Se envio un nuevo correo con las credenciales 🙂');
        }

        return $this->redirectToRoute('app_vendedorn_clientes', [
            'id' => $idcuenta
        ]);
    }

    /**
     * @Route ("/cuentas/{idcuenta}/cliente/{id}/editar", name="app_vendedorn_cliente_editar")
     */
    public function editClienteCuenta(EntityManagerInterface $entityManager, ClienteRepository $clienteRepository,
                                      Request $request, $idcuenta, $id)
    {
        $cliente = $clienteRepository->find($id);

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($request->getMethod() == Request::METHOD_POST) {
            if ($request->request->get('email') == '') {
                $this->addFlash('error', 'Los campos marcados en asterisco son necesarios 🙁');
            } else {

                $cliente->setUser($user);
                $cliente->setEmail($request->request->get('email'));
                $cliente->setNombre($request->request->get('nombre'));
                $cliente->setApellido($request->request->get('apellido'));
                $cliente->setDescription($request->request->get('descripcion'));

                $em->flush();

                $this->addFlash('success', 'Cliente editado correctamente 🙂');
                return $this->redirectToRoute('app_vendedorn_clientes', [
                    'id' => $idcuenta
                ]);
            }
        }

        return $this->render('default/edit.html.twig', [
            'cliente' => $cliente
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     * @Route("/cuenta/solicitar", name="app_vendedor_solicitar_cuenta")
     */
    public function solicitarCuenta(EntityManagerInterface $entityManager, PlanRepository $planRepository, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();

        $planes = $planRepository->findBy([], ['precio' => 'ASC']);

        if ($request->getMethod() === Request::METHOD_POST) {

            if ($request->request->has('idplan') || !empty($request->request->get('idplan'))) {
                $idplan = $request->request->get('idplan');
                $plantemp = $planRepository->find($idplan);
                if ($plantemp != null) {
                    if ($user->getSaldo() >= $plantemp->getPrecio()) {
                        $cuenta = new Cuenta();
                        $cuenta->setUser($user);
                        $cuenta->setCreated(new \DateTime());
                        $cuenta->setEstado(false);
                        $cuenta->setPlan($plantemp);
                        $cuenta->setRestanteCliente($plantemp->getNumeroClientes());
                        $entityManager->persist($cuenta);

                        //-- Restamos el saldo al vendedor
                        $user->setSaldo($user->getSaldo() - $plantemp->getPrecio());
                        $entityManager->persist($user);
                        $entityManager->flush();
                        $this->addFlash('success', 'Se solicito la cuenta con éxito, por favor espere el proceso. 😃');
                        return $this->redirectToRoute('app_vendedorn');
                    } else {
                        $this->addFlash('error', 'Su saldo no es suficiente para el plan seleccionado, Por favor recarque su cuenta 🙁');
                    }

                } else {
                    $this->addFlash('error', 'Error de integridad de datos ☹');
                }
            } else {
                $this->addFlash('error', 'Error de integridad de datos ☹');
            }

            /*$cuenta = new Cuenta();
            $cuenta->setUser($user);
            $cuenta->setCreated(new \DateTime());
            $cuenta->setEstado(false);
            $entityManager->persist($cuenta);
            $entityManager->flush();

            $this->addFlash('success', 'Se solicito la cuenta con éxito 🙂');*/
        }

        // return $this->redirectToRoute('app_vendedorn');

        return $this->render('default/plan_cliente.html.twig', [
            'planes' => $planes,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/clientes-anteriores", name="app_vendedor_clientes_anterior")
     * @param ClienteRepository $clienteRepository
     * @return Response
     */
    public function misClientes(ClienteRepository $clienteRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();
        $clientes = $clienteRepository->findBy(['user' => $user], ['id' => 'DESC']);

        return $this->render('default/anterior_clientes.html.twig', [
            'clientes' => $clientes,
        ]);
    }

}