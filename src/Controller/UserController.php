<?php


namespace App\Controller;


use App\Entity\Demo;
use App\Entity\PerfilUsuario;
use App\Entity\User;
use App\Form\UserType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Twig\Profiler\Profile;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/usuario")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/registrar", name="user_register")
     */
    public function registro(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');


        $user_log = $this->getUser();


        $usuarios_demo = '';
        if ($request->getMethod() == Request::METHOD_POST) {
            if ($request->request->get('password') != $request->request->get("repeatPassword")) {
                $this->addFlash('error', 'Las constraseñas no coinciden');
            } else {
                foreach ($request->request->get('usuario') as $key => $value) {
                    $usuarios_demo .= $value . ',';
                }

                $password_demo = $request->request->get('password_demo');
                $usuarios_demo = substr($usuarios_demo, 0, -1);

                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                try {
                    $user = new User();
                    $perfil = new PerfilUsuario();
                    $demo = new Demo();
                    $user->setEmail($request->request->get('email'));
                    $passwordPlain = $request->request->get('password');
                    $password = $passwordEncoder->encodePassword($user, $passwordPlain);
                    $user->setPassword($password);
                    $roles = $request->request->get('roles');
                    $user->setRoles([$roles]);
                    $user->setIsActive($request->request->get('status'));
                    $user->setPatrocinador($user_log);
                    $user->setPerfil($perfil);
                    $em->persist($user);
                    $demo->setUsuario($usuarios_demo);
                    $demo->setUser($user);
                    $demo->setPassword($password_demo);
                    $em->persist($demo);

                    $em->flush();
                    $em->getConnection()->commit();
                } catch (\Exception $e) {
                    $em->getConnection()->rollBack();
                    throw $e;
                }
                return $this->redirectToRoute('user_vendedor');
            }
        }

        return $this->render('user/register.html.twig', [
            'usuarios' => $usuarios_demo,
        ]);
    }

    /**
     * @Route("/perfil", name="user_profile")
     */
    public function perfil(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        return $this->render('user/perfil.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/editar", name="user_profile_edit")
     */
    public function editar(Request $request, FileUploader $fileUploader)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $u = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($u->getId());

        $profile = $em->getRepository(PerfilUsuario::class)
            ->find($user->getPerfil()->getId());

        if ($user) {
            if ($request->getMethod() == Request::METHOD_POST) {

                if ($request->files->get('foto')) {
                    $urlFoto = $request->files->get('foto');
                    $foto = $fileUploader->upload($urlFoto, 'perfil');
                    $profile->setFoto($foto);
                }

                $user->setFirstName($request->request->get('firstName'));
                $user->setLastName($request->request->get('lastName'));
                /*$user->setPatrocinador($u);*/
                $profile->setWhatsapp($request->request->get('whatsapp'));
                $profile->setDireccion($request->request->get('direccion'));
                $profile->setPais($request->request->get('pais'));
                $profile->setUrlFacebook($request->request->get('urlFacebook'));
                $profile->setUrlInstagram($request->request->get('urlInstagram'));
                $profile->setUrlYoutube($request->request->get('urlYoutube'));
                $em->flush();

                return $this->redirectToRoute('user_profile');
            }
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * @Route("/vendedores/{page<[1-9]\d*>}", name="user_vendedor")
     */
    public function vendedores(Request $request, $page = 1)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $email = null;
        $status = null;
        if ($request->query->has('email')) $email = $request->query->get('email');
        if ($request->query->has('status')) $status = $request->query->get('status');


        $paginator = $this->getDoctrine()->getRepository('App:User')
            ->findAllVendedores($page, $email, $status);

        return $this->render('user/vendedores.html.twig', [
            'paginator' => $paginator
        ]);
    }

}