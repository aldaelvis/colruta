<?php

namespace App\Controller;

use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrdenController
 * @package App\Controller
 * @Route("/admin/pedidos")
 */
class OrdenController extends AbstractController
{
    /**
     * @Route("/{page<[1-9]\d*>}", name="pedidos")
     */
    public function index(Request $request, $page = 1)
    {

        $status = null;
        if ($request->query->has('status')) {
            $status = $request->query->get('status');
        }

        $em = $this->getDoctrine()->getManager();
        $paginator = $em->getRepository('App:Order')->findAllOrden($page, $status);

        return $this->render('pedidos/index.html.twig', [
            'paginator' => $paginator,
        ]);
    }

    /**
     * @Route("/{id}/active", name="orden_active")
     */
    public function activeOrder(Request $request, Order $order)
    {
        $em = $this->getDoctrine()->getManager();
        if ($order) {
            $order->setStatus('ACEPTADO');
            $user = $order->getUser();
            $user->setSaldo($user->getSaldo() + $order->getSaldo());
            $em->flush();
        }
        return $this->redirectToRoute('pedidos');
    }
}
