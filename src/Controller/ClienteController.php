<?php

namespace App\Controller;

use App\Entity\Cliente;
use App\Repository\ClienteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ClienteController extends AbstractController
{
    /**
     * @Route ("/admin/cliente", name="admin_cliente")
     * @Route("/admin/cliente/page/{page<[1-9]\d*>}", name="admin_cliente_paginate")
     */
    public function index(Request $request, ClienteRepository $clienteRepository, $page = 1)
    {

        $email = null;
        if ($request->query->has('email')) {
            $email = $request->query->get('email');
        }

        $paginator = $clienteRepository->findAllClientes($page, $email);

        return $this->render('cliente/index.html.twig', [
            'paginator' => $paginator,
        ]);
    }

    /**
     * @Route("/admin/cliente/registrar", name="admin_cliente_registrar")
     */
    public function registrar(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cliente = new Cliente();
        $user = $this->getUser();

        if ($request->getMethod() == Request::METHOD_POST) {
            if ($request->request->get('email') == '' && $request->request->get('password') == '') {
                $this->addFlash('error', 'Los campos marcados en asterisco son necesarios');
            } else {
                $cliente->setUser($user);
                $cliente->setEmail($request->request->get('email'));
                $cliente->setCliente($request->request->get('cliente'));
                $cliente->setPassword($request->request->get('password'));
                $cliente->setCelular($request->request->get('celular'));
                $cliente->setNombre($request->request->get('nombre'));
                $cliente->setApellido($request->request->get('apellido'));
                $cliente->setDescription($request->request->get('descripcion'));
                $cliente->setUpdateClient((new \DateTime())->modify('+30 days'));
                $em->persist($cliente);
                $em->flush();

                return $this->redirectToRoute('admin_cliente');
            }
        }

        return $this->render('cliente/register.html.twig', [
            'controller_name' => 'ClienteController',
        ]);
    }

    //Todo Modificar el correo

    /**
     * @Route("/admin/cliente/{id}/edit", name="admin_cliente_edit")
     */
    public function editClient(Request $request, \Swift_Mailer $mailer, Cliente $cliente)
    {

        $em = $this->getDoctrine()->getManager();

        if ($request->getMethod() == Request::METHOD_POST) {
            if ($request->request->get('email') == '' && $request->request->get('password') == '') {
                $this->addFlash('error', 'Los campos marcados en asterisco son necesarios');
            } else {
                $cliente->setEmail($request->request->get('email'));
                $cliente->setCliente($request->request->get('cliente'));
                $cliente->setPassword($request->request->get('password'));
                $cliente->setNombre($request->request->get('nombre'));
                $cliente->setApellido($request->request->get('apellido'));
                $cliente->setDescription($request->request->get('descripcion'));
                if ($request->request->get('aumentar')) {
                    $cliente->setUpdateClient((new \DateTime())->modify('+30 days'));
                }

                $em->flush();

                $message = (new \Swift_Message('Accesos para tu cuenta'))
                    ->setFrom('info@4kgoiptv.com')
                    ->setTo($cliente->getEmail())
                    ->setBody(
                        $this->renderView(
                        // templates/emails/registration.html.twig
                            'emails/message.html.twig',
                            [
                                'email' => $cliente->getEmail(),
                                'password' => $cliente->getPassword()
                            ]
                        ),
                        'text/html'
                    );
                $mailer->send($message);

                return $this->redirectToRoute('admin_cliente');
            }
        }

        return $this->render('cliente/edit.html.twig', [
            'cliente' => $cliente,
        ]);
    }

    /**
     * @Route("/admin/cliente/{id}", name="admin_cliente_show")
     */
    public function showClient(Cliente $cliente)
    {
        return $this->render('cliente/show.html.twig', [
            'cliente' => $cliente,
        ]);
    }

    /**
     * @Route("/admin/cliente-vence", name="admin_cliente_vence")
     */
    public function clienteVence()
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository('App:Cliente')->findAllClienToExpired();

        return $this->render('cliente/clientes_vence.html.twig', [
            'clientes' => $clientes,
        ]);
    }
}
