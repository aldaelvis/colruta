<?php


namespace App\Controller;


use App\Entity\Cliente;
use App\Entity\Cuenta;
use App\Entity\DetalleCuenta;
use App\Entity\Order;
use App\Entity\PerfilUsuario;
use App\Entity\User;
use App\Repository\ClienteRepository;
use App\Repository\ConfiguracionRepository;
use App\Repository\CuentaRepository;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController extends AbstractController
{

    /**
     * @Route("/entrenamiento/{categoria}", name="entrenamiento")
     */
    public function entrenamiento(Request $request, $categoria = '')
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('App:Categoria')->findAll();


        if ($categoria != '') {
            $tutoriales = $em->getRepository('App:Tutorial')->findByCategorieTutorial($categoria);
        } else {
            $tutoriales = $em->getRepository('App:Tutorial')->findAll();
        }

        return $this->render('default/entrenamiento.html.twig', [
            'categorias' => $categorias,
            'tutoriales' => $tutoriales,
        ]);
    }

    /**
     * @Route("/cliente/registrar", name="cliente_registrar")
     */
    public function registrar(EntityManagerInterface $entityManager, UserRepository $userRepository, CuentaRepository $cuentaRepository,
                              \Swift_Mailer $mailer, Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        /** @var User $user */
        $user = $this->getUser();
        $cuentas = $userRepository->showCuentasNoVencidas($user->getId());

        if ($request->getMethod() == Request::METHOD_POST) {

            if ($request->request->has('email') && $request->request->has('celular')
                && $request->request->has('idcuenta')) {

                $email = $request->request->get('email');
                $celular = $request->request->get('celular');
                $idcuenta = $request->request->get('idcuenta');
                $nombre = $request->request->get('nombre');
                $apellido = $request->request->get('apellido');
                $descripcion = $request->request->get('descripcion');

                $entityManager->getConnection()->beginTransaction();
                try {

                    //--validamos que la cuenta le pertenesca al usuario logueado
                    $cuentatemp = null;
                    $pass = false;
                    foreach ($cuentas as $value) {
                        if ($idcuenta == $value["id"]) {
                            $cuentatemp = $cuentaRepository->find($idcuenta);
                            if ($cuentatemp->getRestanteCliente() > 0) {
                                $cuentatemp->setRestanteCliente($cuentatemp->getRestanteCliente() - 1);
                                $entityManager->persist($cuentatemp);
                                $pass = true;
                            } else {
                                $pass = false;
                                $this->addFlash('error', 'La cuenta no tiene mas cliente para registrar 🙁');
                            }
                        }
                    }
                    if ($pass) {
                        $contador = 0;
                        //--persistir los clientes|detalles
                        while ($contador < count($request->request->get('email'))) {
                            $cliente = new Cliente();
                            $cliente->setEmail($email[$contador]);
                            $cliente->setCelular($celular[$contador]);
                            $cliente->setDescription($descripcion[$contador]);
                            $cliente->setNombre($nombre[$contador]);
                            $cliente->setApellido($apellido[$contador]);
                            $cliente->setCliente($cuentatemp->getUsername());
                            $cliente->setPassword($cuentatemp->getPassword());
                            $cliente->setUpdateClient(new \DateTime());
                            $detalle = new DetalleCuenta();
                            $detalle->setCliente($cliente);
                            $detalle->setCuenta($cuentatemp);
                            $detalle->setCreated(new \DateTime());
                            $entityManager->persist($cliente);
                            $entityManager->persist($detalle);

                            $message = (new \Swift_Message('Accesos para tu cuenta'))
                                ->setFrom('info@4kgoiptv.com')
                                ->setTo($email[$contador])
                                ->setBody(
                                    $this->renderView(
                                    // templates/emails/registration.html.twig
                                        'emails/message.html.twig',
                                        [
                                            'email' => $cuentatemp->getUsername(),
                                            'password' => $cuentatemp->getPassword()
                                        ]
                                    ),
                                    'text/html'
                                );
                            $mailer->send($message);

                            $contador++;
                        }

                        $entityManager->flush();
                        $entityManager->getConnection()->commit();


                        $this->addFlash('success', 'Los clientes se guardaron y se enviarons los correos correctamente 😀');
                    } else {
                        $this->addFlash('error', 'Error de integridad en los campos 🙁');
                    }

                    return $this->redirectToRoute('app_vendedorn');
                } catch (\Exception $e) {
                    $entityManager->rollback();
                    throw $e;
                }

            } else {
                $this->addFlash('error', 'Agregue clientes y/o los campos marcados en asterisco son necesarios 🙁');
            }
        }

        return $this->render('default/register.html.twig', [
            'cuentas' => $cuentas,
        ]);
    }

    /**
     * @Route("/cliente", name="cliente")
     */
    public function index(ClienteRepository $clienteRepository, CuentaRepository $cuentaRepository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        /** @var User $user */
        $user = $this->getUser();

        /** @var Cuenta $cuentas */
        $cuentas = $cuentaRepository->findBy([
            'user' => $user,
        ], ['id' => 'DESC']);

        /** @var DetalleCuenta $detalles */
        $detalles = $cuentas->getDetalleCuentas();

        $clientes = $detalles->getCliente();

        return $this->render('default/listado_clientes.html.twig', [
            'clientes' => $clientes,
        ]);
    }

//    /**
//     * @Route("/cliente/{id}/edit", name="cliente_edit", methods={"GET","POST"})
//     */
    /*public function edit(Request $request, Cliente $cliente): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($request->getMethod() == Request::METHOD_POST) {
            if ($request->request->get('email') == '') {
                $this->addFlash('error', 'Los campos marcados en asterisco son necesarios');
            } else {

                $cliente->setUser($user);
                $cliente->setEmail($request->request->get('email'));
                $cliente->setNombre($request->request->get('nombre'));
                $cliente->setApellido($request->request->get('apellido'));
                $cliente->setDescription($request->request->get('descripcion'));

                $em->flush();

                return $this->redirectToRoute('cliente');
            }
        }

        return $this->render('default/edit.html.twig', [
            'cliente' => $cliente
        ]);
    }*/


    /**
     * @Route("/patrocinador", name="patrocinador", methods={"GET"})
     */
    public function patrocinador(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        return $this->render('default/patrocinador.html.twig', [
            'patrocinador' => $user
        ]);
    }

    /**
     * @Route("/vendedores", name="vendedores", methods={"GET"})
     */
    public function vendedores(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user_log = $this->getUser();

        //$user = $this->getDoctrine()->getRepository('App:User')->findVendedoresById($user_log->getId());

        $em = $this->getDoctrine()->getManager();
        $numero_telefono_usuario = $em->getRepository(User::class)->Verificar_Numero($user_log->getId());

        return $this->render('default/vendedor_listado.html.twig', [
            'vendedores' => $numero_telefono_usuario,
        ]);
    }

    /**
     * @Route("/vendedor/registrar", name="vendedor_registrar")
     * @param ConfiguracionRepository $configuracionRepository
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     */
    public function registro(ConfiguracionRepository $configuracionRepository, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        //Constante de costo de crear usuario
        $config = $configuracionRepository->findOneBy([], ['id' => 'DESC']);
        $costo = $config->getPrecioUsuario();
        $user = new User();

        /** @var User $user_log */
        $user_log = $this->getUser();

        $perfil = new PerfilUsuario();
        $saldo = $user_log->getSaldo();

        if ($saldo >= $costo) {
            $pass = true;
        } else {
            $pass = false;
        }

        if ($request->getMethod() == Request::METHOD_POST) {
            if ($request->request->get('password') != $request->request->get("repeatPassword")) {
                $this->addFlash('error', 'Las constraseñas no coinciden');
            } else {
                if ($pass) {
                    $user->setEmail($request->request->get('email'));
                    $passwordPlain = $request->request->get('password');
                    $password = $passwordEncoder->encodePassword($user, $passwordPlain);
                    $user->setPassword($password);
                    $user->setRoles(['ROLE_REVENDEDOR']);
                    $user->setIsActive($request->request->get('status'));
                    $user->setPatrocinador($user_log);
                    $user->setPerfil($perfil);
                    $em = $this->getDoctrine()->getManager();

                    $user_log->setSaldo($saldo - $costo);
                    $em->persist($user);
                    $em->flush();

                    return $this->redirectToRoute('vendedores');
                }
            }
        }

        return $this->render('default/vendedor_registrar.html.twig', [
            'saldo' => $saldo,
            'pass' => $pass
        ]);
    }

    /**
     * @Route("/vendedor/{id}/edit", name="vendedor_editar")
     */
    public function editarVendedor(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($request->getMethod() == Request::METHOD_POST) {
            if ($request->request->get('email') == '') {
                $this->addFlash('error', 'El email es requerido');
            } else {
                $user->setEmail($request->request->get('email'));
                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return $this->redirectToRoute('vendedores');
            }
        }

        return $this->render('default/vendedor_editar.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/vendedor/{id}", name="vendedor_show")
     */
    public function showVendedor(Request $request, User $user)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('default/vendedor_show.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/recargar", name="vendedor_recargar")
     */
    public function recargarVendedor(Request $request, FileUploader $fileUploader)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $order = new Order();
        $user = $this->getUser();
        if ($request->getMethod() == Request::METHOD_POST) {
            $urlData = $request->files->get('voucher');
            $foto = $fileUploader->upload($urlData, 'vourches');
            $order->setUser($user);
            $order->setSaldo($request->request->get('saldo'));
            $order->setUrl($foto);

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();
            return $this->redirectToRoute('vendedor_recargas');
        }

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $recargar = $em->getRepository(User::class)->ConsultarSaldo($user->getId());

        return $this->render('default/recargar_vendedor.html.twig', [
            'rec' => $recargar,
        ]);
    }

    /**
     * @Route("/recargas", name="vendedor_recargas")
     */
    public function showOrders(Request $request, FileUploader $fileUploader)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $orders = $em->getRepository('App:Order')->findBy([
            'user' => $user
        ]);


        return $this->render('default/recargas.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route("/recarga/{id}", name="vendedor_recarga")
     */
    public function showOrder(Request $request, Order $order)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('default/recarga_show.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/recarga/{id}/delete", name="vendedor_cancel_recarga")
     */
    public function cancelOrder(Request $request, Order $order)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entityManager = $this->getDoctrine()->getManager();
        $order->setStatus('CANCELADO');
        $entityManager->flush();
        return $this->redirectToRoute('vendedor_recargas');
    }


    /**
     * @Route("/iptv", name="ver_iptv")
     */
    public function iptv()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $demos = $entityManager->getRepository('App:Demo')->findOneBy([
            'user' => $user,
        ]);

        $usuarios = explode(",", $demos->getUsuario());

        return $this->render('default/iptv.html.twig', [
            'demos' => $demos,
            'usuarios' => $usuarios,
        ]);
    }


}