<?php

namespace App\Controller;

use App\Entity\Configuracion;
use App\Repository\ConfiguracionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConfiguracionController extends AbstractController
{
    /**
     * @Route("admin/configuracion", name="configuracion")
     * @param ConfiguracionRepository $configuracionRepository
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     */
    public function index(ConfiguracionRepository $configuracionRepository, EntityManagerInterface $entityManager,
                          Request $request)
    {

        $config = $configuracionRepository->findOneBy([], ['id' => 'DESC']);

        if ($request->getMethod() === Request::METHOD_POST) {
            $precio = $request->request->get('precio_vendedor');
            $id = $request->request->get('id');
            if (empty($precio)) {
                $this->addFlash('error', 'Debe de ingresar un precio 🙁');
            } else {
                if (!empty($id)) {
                    $con = $configuracionRepository->find($id);
                    $con->setPrecioUsuario($precio);
                    $this->addFlash('success', 'Se actualizo correctamente 😃');
                } else {
                    $con = new Configuracion();
                    $con->setPrecioUsuario($precio);
                    $this->addFlash('success', 'Se inserto correctamente 😃');
                }
                $entityManager->persist($con);
                $entityManager->flush();
            }
        }

        return $this->render('configuracion/index.html.twig', [
            'config' => $config,
        ]);
    }
}
