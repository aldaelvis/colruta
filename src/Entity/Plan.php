<?php

namespace App\Entity;

use App\Repository\PlanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlanRepository::class)
 */
class Plan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column (type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $precio;

    /**
     * @ORM\Column (type="integer")
     */
    private $numero_clientes;

    /**
     * @ORM\OneToMany(targetEntity=Cuenta::class, mappedBy="plan")
     */
    private $cuentas;

    public function __construct()
    {
        $this->cuentas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio): void
    {
        $this->precio = $precio;
    }

    /**
     * @return Collection|Cuenta[]
     */
    public function getCuentas(): Collection
    {
        return $this->cuentas;
    }

    public function addCuenta(Cuenta $cuenta): self
    {
        if (!$this->cuentas->contains($cuenta)) {
            $this->cuentas[] = $cuenta;
            $cuenta->setPlan($this);
        }

        return $this;
    }

    public function removeCuenta(Cuenta $cuenta): self
    {
        if ($this->cuentas->contains($cuenta)) {
            $this->cuentas->removeElement($cuenta);
            // set the owning side to null (unless already changed)
            if ($cuenta->getPlan() === $this) {
                $cuenta->setPlan(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumeroClientes()
    {
        return $this->numero_clientes;
    }

    /**
     * @param mixed $numero_clientes
     */
    public function setNumeroClientes($numero_clientes): void
    {
        $this->numero_clientes = $numero_clientes;
    }

}
