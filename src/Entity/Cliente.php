<?php

namespace App\Entity;

use App\Repository\ClienteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClienteRepository::class)
 */
class Cliente
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $cliente;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="clients")
     */
    private $user;

    /**
     * @ORM\Column(type="text", length=50, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="text", length=16, nullable=true)
     */
    private $celular;

    /**
     * @ORM\Column(type="date")
     */
    private $createdClient;

    /**
     * @ORM\Column(type="date")
     */
    private $updateClient;

    /**
     * @ORM\OneToMany(targetEntity=DetalleCuenta::class, mappedBy="cliente")
     */
    private $detalleCuentas;


    public function __construct()
    {
        $this->createdClient = new \DateTime();
        $this->detalleCuentas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(?string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCliente(): ?string
    {
        return $this->cliente;
    }

    public function setCliente(string $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular): void
    {
        $this->celular = $celular;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedClient(): \DateTime
    {
        return $this->createdClient;
    }

    /**
     * @param \DateTime $createdClient
     */
    public function setCreatedClient(\DateTime $createdClient): void
    {
        $this->createdClient = $createdClient;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateClient(): \DateTime
    {
        return $this->updateClient;
    }

    /**
     * @param \DateTime $updateClient
     */
    public function setUpdateClient(\DateTime $updateClient): void
    {
        $this->updateClient = $updateClient;
    }

    /**
     * @return Collection|DetalleCuenta[]
     */
    public function getDetalleCuentas(): Collection
    {
        return $this->detalleCuentas;
    }

    public function addDetalleCuenta(DetalleCuenta $detalleCuenta): self
    {
        if (!$this->detalleCuentas->contains($detalleCuenta)) {
            $this->detalleCuentas[] = $detalleCuenta;
            $detalleCuenta->setCliente($this);
        }

        return $this;
    }

    public function removeDetalleCuenta(DetalleCuenta $detalleCuenta): self
    {
        if ($this->detalleCuentas->contains($detalleCuenta)) {
            $this->detalleCuentas->removeElement($detalleCuenta);
            // set the owning side to null (unless already changed)
            if ($detalleCuenta->getCliente() === $this) {
                $detalleCuenta->setCliente(null);
            }
        }

        return $this;
    }


}
