<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PerfilUsuario
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="perfil")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $foto;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $whatsapp;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $pais;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $urlFacebook;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $urlInstagram;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $urlYoutube;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }


    /**
     * @return mixed
     */
    public function getWhatsapp()
    {
        return $this->whatsapp;
    }

    /**
     * @param mixed $whatsapp
     */
    public function setWhatsapp($whatsapp): void
    {
        $this->whatsapp = $whatsapp;
    }

    /**
     * @return mixed
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * @param mixed $pais
     */
    public function setPais($pais): void
    {
        $this->pais = $pais;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion): void
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getUrlFacebook()
    {
        return $this->urlFacebook;
    }

    /**
     * @param mixed $urlFacebook
     */
    public function setUrlFacebook($urlFacebook): void
    {
        $this->urlFacebook = $urlFacebook;
    }

    /**
     * @return mixed
     */
    public function getUrlInstagram()
    {
        return $this->urlInstagram;
    }

    /**
     * @param mixed $urlInstagram
     */
    public function setUrlInstagram($urlInstagram): void
    {
        $this->urlInstagram = $urlInstagram;
    }

    /**
     * @return mixed
     */
    public function getUrlYoutube()
    {
        return $this->urlYoutube;
    }

    /**
     * @param mixed $urlYoutube
     */
    public function setUrlYoutube($urlYoutube): void
    {
        $this->urlYoutube = $urlYoutube;
    }

    public function __toString()
    {
        return $this->whatsapp;
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto($foto): void
    {
        $this->foto = $foto;
    }

}