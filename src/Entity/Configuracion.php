<?php

namespace App\Entity;

use App\Repository\ConfiguracionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfiguracionRepository::class)
 */
class Configuracion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $precio_usuario;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrecioUsuario(): ?string
    {
        return $this->precio_usuario;
    }

    public function setPrecioUsuario(string $precio_usuario): self
    {
        $this->precio_usuario = $precio_usuario;

        return $this;
    }
}
