<?php

namespace App\Entity;

use App\Repository\CuentaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CuentaRepository::class)
 */
class Cuenta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $password;

    /**
     * @ORM\ManyToOne(targetEntity=Plan::class, inversedBy="cuentas")
     */
    private $plan;

    /**
     * @ORM\Column(type="date")
     */
    private $created;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $vencimiento;

    /**
     * @ORM\OneToMany(targetEntity=DetalleCuenta::class, mappedBy="cuenta")
     */
    private $detalleCuentas;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="cuentas")
     */
    private $user;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $restante_cliente;

    /**
     * @ORM\Column(type="boolean")
     */
    private $estado;


    public function __construct()
    {
        $this->detalleCuentas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getVencimiento(): ?\DateTimeInterface
    {
        return $this->vencimiento;
    }

    public function setVencimiento(\DateTimeInterface $vencimiento): self
    {
        $this->vencimiento = $vencimiento;

        return $this;
    }

    /**
     * @return Collection|DetalleCuenta[]
     */
    public function getDetalleCuentas(): Collection
    {
        return $this->detalleCuentas;
    }

    public function addDetalleCuenta(DetalleCuenta $detalleCuenta): self
    {
        if (!$this->detalleCuentas->contains($detalleCuenta)) {
            $this->detalleCuentas[] = $detalleCuenta;
            $detalleCuenta->setCuenta($this);
        }

        return $this;
    }

    public function removeDetalleCuenta(DetalleCuenta $detalleCuenta): self
    {
        if ($this->detalleCuentas->contains($detalleCuenta)) {
            $this->detalleCuentas->removeElement($detalleCuenta);
            // set the owning side to null (unless already changed)
            if ($detalleCuenta->getCuenta() === $this) {
                $detalleCuenta->setCuenta(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRestanteCliente()
    {
        return $this->restante_cliente;
    }

    /**
     * @param mixed $restante_cliente
     */
    public function setRestanteCliente($restante_cliente): void
    {
        $this->restante_cliente = $restante_cliente;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

}
