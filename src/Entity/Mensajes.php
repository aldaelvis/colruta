<?php

namespace App\Entity;

use App\Repository\MensajesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MensajesRepository::class)
 *
 */
class Mensajes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups ({"notificacion"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups ({"notificacion"})
     */
    private $titulo;

    /**
     * @ORM\Column(type="text")
     * @Groups ({"notificacion"})
     */
    private $descripcion;

    /**
     * @ORM\Column(type="boolean")
     * @Groups ("{notificacion}")
     */
    private $estado;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="mensajes")
     * @Groups ("{notificacion}")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups ("{notificacion}")
     */
    private $remitente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getEstado(): ?bool
    {
        return $this->estado;
    }

    public function setEstado(bool $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRemitente(): ?string
    {
        return $this->remitente;
    }

    public function setRemitente(string $remitente): self
    {
        $this->remitente = $remitente;

        return $this;
    }
}
