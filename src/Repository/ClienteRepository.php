<?php

namespace App\Repository;

use App\Entity\Cliente;
use App\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cliente|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cliente|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cliente[]    findAll()
 * @method Cliente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClienteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cliente::class);
    }


    public function findAllClientes($page, $email = null)
    {
        $dq = $this->createQueryBuilder('c')
            ->join('c.user', 'u')
            ->orderBy('c.id', 'DESC');

        if (null !== $email) {
            $dq->andWhere('c.email LIKE :email')
                ->setParameter('email', '%' . $email . '%');
        }

        return (new Paginator($dq))->paginate($page);
    }

    // /**
    //  * @return Cliente[] Returns an array of Cliente objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cliente
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllClienToExpired()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT DATE_DIFF(CURRENT_DATE(),c.updateClient) as total, c FROM App\Entity\Cliente c
            WHERE CURRENT_DATE() >= c.updateClient ORDER BY c.updateClient DESC"
        );

        return $query->getResult();
    }
}
