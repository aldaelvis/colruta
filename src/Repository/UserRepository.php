<?php

namespace App\Repository;

use App\Entity\User;
use App\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function ConsultarSaldo($user)
    {

        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT saldo
                FROM user Tusu
                WHERE Tusu. id = $user";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function Verificar_Numero($user)
    {

        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT * 
                FROM user Tusu
                INNER JOIN perfil_usuario Tper
                ON Tusu. id = Tper. user_id
                WHERE Tusu. patrocinador = $user
            ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        // var_dump($stmt->fetchAll());
        return $stmt->fetchAll();
    }
    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findVendedoresById($value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.patrocinador = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }


    public function findAllVendedores($page, $email, $status)
    {
        $dq = $this->createQueryBuilder('u')
            ->orderBy('u.id', 'DESC');

        if (null !== $status) {
            $dq->where('u.isActive LIKE :status')
                ->setParameter('status', '%' . $status . '%');
        }

        if ($email !== null) {
            $dq->andWhere('u.email LIKE :email')
                ->setParameter('email', '%' . $email . '%');
        }

        return (new Paginator($dq))->paginate($page);
    }

    //--Mostrar todas las cuentas que no esten vencidos
    public function showCuentasNoVencidas($id)
    {
        $dq = $this->createQueryBuilder('u')
            ->join('u.cuentas', 'c')
            ->select(['c.id', 'c.created', 'c.vencimiento',
                'c.username', 'c.vencimiento', 'c.password', 'c.restante_cliente'])
            ->where('c.created < c.vencimiento')
            ->andwhere('u.id = :id')
            ->andWhere('c.restante_cliente > 0')
            ->andWhere('c.estado != 0')
            ->setParameter('id', $id);

        return $dq
            ->getQuery()
            ->getResult();
    }
}
