<?php

namespace App\Repository;

use App\Entity\DetalleCuenta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DetalleCuenta|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetalleCuenta|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetalleCuenta[]    findAll()
 * @method DetalleCuenta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetalleCuentaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetalleCuenta::class);
    }

    // /**
    //  * @return DetalleCuenta[] Returns an array of DetalleCuenta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetalleCuenta
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByCuenta($id)
    {
        return $this->createQueryBuilder('d')
            ->join("d.cliente", "cl")
            ->join("d.cuenta", "cu")
            ->where('cu.id = :id')
            ->setParameter('id', $id)
            ->orderBy('d.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
