<?php

namespace App\Repository;

use App\Entity\Cuenta;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cuenta|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cuenta|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cuenta[]    findAll()
 * @method Cuenta[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CuentaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cuenta::class);
    }

    // /**
    //  * @return Cuenta[] Returns an array of Cuenta objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findAllCuentaToExpired($id)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT DATE_DIFF(CURRENT_DATE(),  c.vencimiento) as total, c FROM App\Entity\Cuenta c
            WHERE CURRENT_DATE() >= c.vencimiento AND c.user = :id ORDER BY c.vencimiento DESC"
        )->setParameter('id', $id);

        return $query->getResult();
    }

    /**
     * @return Cuenta[] Returns an array of Cuenta objects
     */
    public function findCuentasSolicitadas()
    {
        return $this->createQueryBuilder('c')
            ->join('c.user', 'u')
            ->andWhere('c.estado = 0')
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
