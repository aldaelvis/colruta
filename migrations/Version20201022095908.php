<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201022095908 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cuenta CHANGE username username VARCHAR(50) DEFAULT NULL, CHANGE password password VARCHAR(50) DEFAULT NULL, CHANGE vencimiento vencimiento DATE DEFAULT NULL, CHANGE restante_cliente restante_cliente INT DEFAULT NULL');
        //$this->addSql('ALTER TABLE user DROP INDEX user_user_id_fk, ADD UNIQUE INDEX UNIQ_8D93D649C57427E1 (patrocinador)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cuenta CHANGE username username VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE vencimiento vencimiento DATE NOT NULL, CHANGE restante_cliente restante_cliente INT NOT NULL');
        //$this->addSql('ALTER TABLE user DROP INDEX UNIQ_8D93D649C57427E1, ADD INDEX user_user_id_fk (patrocinador)');
    }
}
