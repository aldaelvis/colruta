<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201021141226 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mensajes (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, titulo VARCHAR(100) NOT NULL, descripcion LONGTEXT NOT NULL, estado TINYINT(1) NOT NULL, remitente VARCHAR(100) NOT NULL, INDEX IDX_6C929C80A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mensajes ADD CONSTRAINT FK_6C929C80A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        //$this->addSql('ALTER TABLE user DROP INDEX user_user_id_fk, ADD UNIQUE INDEX UNIQ_8D93D649C57427E1 (patrocinador)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE mensajes');
        //$this->addSql('ALTER TABLE user DROP INDEX UNIQ_8D93D649C57427E1, ADD INDEX user_user_id_fk (patrocinador)');
    }
}
