<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201022104148 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE detalle_cuenta DROP FOREIGN KEY FK_5D1F5C689AEFF118');
        $this->addSql('ALTER TABLE detalle_cuenta ADD CONSTRAINT FK_5D1F5C689AEFF118 FOREIGN KEY (cuenta_id) REFERENCES cuenta (id) ON DELETE CASCADE');
        //$this->addSql('ALTER TABLE user DROP INDEX user_user_id_fk, ADD UNIQUE INDEX UNIQ_8D93D649C57427E1 (patrocinador)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE detalle_cuenta DROP FOREIGN KEY FK_5D1F5C689AEFF118');
        $this->addSql('ALTER TABLE detalle_cuenta ADD CONSTRAINT FK_5D1F5C689AEFF118 FOREIGN KEY (cuenta_id) REFERENCES cuenta (id)');
        //$this->addSql('ALTER TABLE user DROP INDEX UNIQ_8D93D649C57427E1, ADD INDEX user_user_id_fk (patrocinador)');
    }
}
